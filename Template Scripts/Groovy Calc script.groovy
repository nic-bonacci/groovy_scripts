/*RTPS: {rtp_SubStVar_CurrentYear} {rtp_SubStVar_NextYear} {rtp_SubStVar_EndYear} {rtp_SubStVar_CurrentPeriod}*/
String SubStVar_CurrentYear = rtps.rtp_SubStVar_CurrentYear.member.getName()
String SubStVar_NextYear = rtps.rtp_SubStVar_NextYear
String SubStVar_EndYear = rtps.rtp_SubStVar_EndYear
String SubStVar_CurrentPeriod = rtps.rtp_SubStVar_CurrentPeriod

//Define each dimension as a Set variable to be populated in the cell iterator
Set<String> Years = []
Set<String> Measures = []
Set<String> Periods = []
Set<String> Scenarios = []
Set<String> Entitys = []
Set<String> Versions = ["Working", "Final"] as Set //prepopulate prior to the cell iterator with known additional members
Set<String> Accounts = []

// Iterator to check if each cell in the datagrid(web form) has been edited  
int Count = 0 //count variable to establish if data has been edited or not
operation.grid.dataCellIterator({DataCell cell -> cell.edited}).each { DataCell cell ->
	Count++	
	Years << cell.getMemberName("Year")
	Measures << cell.getMemberName("Measure")
	Periods << cell.getMemberName("Period")
	Entitys << cell.getMemberName("Entity")
	Scenarios << cell.getMemberName("Scenario")
	Versions << cell.getMemberName("Version")
	Accounts << cell.getMemberName("Account")            
}

if(Count >= 1) {
	// Convert Member sets into comma delimited strings to be used in smartpush overrides
	String YearsStr = """\"${Years.join('", "')}\""""
    String MeasuresStr = """\"${Measures.join('", "')}\"""" 
	String PeriodsStr = """\"${Periods.join('", "')}\""""
	String EntitysStr = """\"${Entitys.join('", "')}\""""
    String ScenariosStr = """\"${Scenarios.join('", "')}\""""
	String VersionsStr = """\"${Versions.join('", "')}\""""
    String AccountsStr = """\"${Accounts.join('", "')}\""""
 
//Dynamically create the calc script to be executed  
String Calc_ = """
	FIX($ScenariosStr,$VersionsStr)
    	FIX($PeriodsStr,$YearsStr)
        	/* Do Something */
        ENDFIX
	ENDFIX """
	println("The following calc script was executed by $operation.user.fullName: \n $Calc_")
	return Calc_.toString()
} else {
	println("No data has been modified")
	return
}