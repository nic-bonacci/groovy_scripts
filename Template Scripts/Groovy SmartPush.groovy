//Check web form to see if required smartpush is attached
if(!operation.grid.hasSmartPush("")) 
    return
    
//Define each dimension as a Set variable to be populated in the cell iterator
Set<String> Years = []
Set<String> Measures = []
Set<String> Periods = []
Set<String> Scenarios = []
Set<String> Entitys = []
Set<String> Versions = ["Working", "Final"] as Set //prepopulate prior to the cell iterator with known additional members
Set<String> Accounts = []

// Iterator to check if each cell in the datagrid(web form) has been edited  
int Count = 0 //count variable to establish if data has been edited or not
operation.grid.dataCellIterator({DataCell cell -> cell.edited}).each { DataCell cell ->
	Count++
	Years << cell.getMemberName("Year")
	Measures << cell.getMemberName("Measure")
	Periods << cell.getMemberName("Period")
	Entitys << cell.getMemberName("Entity")
	Scenarios << cell.getMemberName("Scenario")
	Versions << cell.getMemberName("Version")
	Accounts << cell.getMemberName("Accounts")
}

if(Count >= 1) {		
	// Convert Member sets into comma delimited strings to be used in smartpush overrides
	String YearsStr = """\"${Years.join('", "')}\""""
    String MeasuresStr = """\"${Measures.join('", "')}\"""" 
	String PeriodsStr = """\"${Periods.join('", "')}\""""
	String EntitysStr = """\"${Entitys.join('", "')}\""""
    String ScenariosStr = """\"${Scenarios.join('", "')}\""""
	String VersionsStr = """\"${Versions.join('", "')}\""""
    String AccountsStr = """\"${Accounts.join('", "')}\""""
    // list of edited members are printed into the job console log for record of members used in the smartpush
	println("Edited Years: " + YearsStr)
	println("Edited Measures: " + MeasuresStr)
	println("Edited Periods: " + PeriodsStr)
	println("Edited Entities: "+EntitysStr)
    println("Edited Scenarios: " + ScenariosStr)
	println("Edited Versions: " + VersionsStr)
    println("Pushed Accounts: " + AccountsStr)
	//Smartpush execution with dimension member overrides fo reach dimension
  	operation.grid.getSmartPush("").execute( [ "Year" : YearsStr, "Measure" : MeasuresStr, "Period" : PeriodsStr, "Entity" : EntitysStr, "Scenario" : ScenariosStr, "Version" : VersionsStr, "Account" : AccountsStr] )    
} else {
	println("No data has been modified")
	return
}