/*******************************************
* BefExtract.groovy for Metadata Process.
********************************************/

/**
* Before extract custom script. This script will be called before the extract query begins execution.
**/
println("Begin: BefExtract.groovy")
agentAPI.logInfo("Begin: BefExtract.groovy")
println("Event type is"+event)
agentAPI.logInfo("Event type is"+event)
/**
* switch statement to determine which dimension is to be loaded. Dynamically update SQL query.
**/
String Dimension = agentContext.get("INTEGRATION")
String SQLStatement
switch(Dimension) {
	case("WorkCenter"):
		SQLStatement = """SELECT WORKCENTER as "Work Center", 
		'All Work Center' AS "Parent",
		CONCAT(CONCAT(WORKCENTER,' - '),WORKCENTER_DESCRIPTION) AS "Alias: Default",
		'store' AS "Data Storage"
		FROM BH_ADW_INTG.MV_D_PLAN_WORKCENTER""";
		break;
	case("Product"):
		SQLStatement = new File(/..\..\Metadata_EPMAgentData\Metadata_SQL\Product.sql/).text;
		break;
	case("Size"):
		SQLStatement = """SELECT DISTINCT
		SIZEGROUP_CODE AS "Size",
		'Size' AS "Parent"
		FROM BH_ADW_INTG.MV_PLAN_PRODUCT_HIERARCHY
		WHERE SIZEGROUP_CODE IS NOT NULL""";
		break;
	case("Account"):
		SQLStatement = new File(/..\..\Metadata_EPMAgentData\Metadata_SQL\Account.sql/).text;
		break;
	case("Department"):
		SQLStatement = new File(/..\..\Metadata_EPMAgentData\Metadata_SQL\Department.sql/).text;
		break;	
	case("BHPlan2Department"):
		SQLStatement = new File(/..\..\Metadata_EPMAgentData\Metadata_SQL\BHPlan2Department.sql/).text;
		break;
	case("BHPlan2Size"):
		SQLStatement = """SELECT DISTINCT
		SIZEGROUP_CODE AS "Size",
		'Size' AS "Parent"
		FROM BH_ADW_INTG.MV_PLAN_PRODUCT_HIERARCHY
		WHERE SIZEGROUP_CODE IS NOT NULL""";
		break;	
	case("BHPlan2Product"):
		SQLStatement = new File(/..\..\Metadata_EPMAgentData\Metadata_SQL\Product.sql/).text;
		break;		
	case("BHPlan2Customer"):
		SQLStatement = new File(/..\..\Metadata_EPMAgentData\Metadata_SQL\Customer.sql/).text;
		break;
	Default:
		SQLStatement = null;
		break;
}

if (SQLStatement != null && (Dimension != "Rate_Code_IntAg_rul" || Dimension != "Asset_Detail_IntAg_rul")) {
	agentAPI.updateQuery(SQLStatement)
	println("Updating Query to "+SQLStatement)
	agentAPI.logInfo("Updating Query to "+SQLStatement)
} else if (Dimension == "Rate_Code_IntAg_rul" || Dimension == "Asset_Detail_IntAg_rul") {
	return
	} else {
		println "Invalid metadata integration remaining steps will be skipped"
		agentAPI.logError("Invalid metadata integration remaining steps will be skipped")
		agentAPI.skipAction('true')
	}

