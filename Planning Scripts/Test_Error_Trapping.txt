try {
  def arr = 1/0
} 
catch(Exception ex) {
  println ex.toString()
  println ex.getMessage()
  println ex.getStackTrace()
  //throwVetoException("An unknown error occurred. Contact your system administrator")
}
finally {
   println "The final block"
}