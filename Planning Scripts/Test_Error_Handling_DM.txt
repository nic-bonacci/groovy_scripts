//check job status while it is still running
def jobComplete = false

while(jobComplete==false){

	//sleep(3000)
    HttpResponse<String> dmResponseCheck = operation.application.getConnection("LocalTest2").get("/aif/rest/V1/jobs/$processID").asString()
    def jsonCheck = new JSONObject(dmResponseCheck.body)
	def statusCheck = jsonCheck.getInt("status")
    def jobStatusCheck = jsonCheck.getString("jobStatus")
    
    if(jobStatusCheck=="RUNNING")
    {
    sleep(5000)    
    }
    
	else{
    	if(statusCheck!=0){
        	throwVetoException("problem running $dlr with process id $processID, status returned:$ruleStatus")
        }
        if(statusCheck==0){
        	println "Data Load Rule: $dlr"
            println "Job Status: $jobStatusCheck"
            println "Process ID: $processID"
        }
	jobComplete=true     
    }
}