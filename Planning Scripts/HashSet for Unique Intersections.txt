hashset to create unique intersections for calc script


/* RTPS: {Adj_PlanElement} */
String quoteTrim(String str) {
    str = str.replace(“\”“,”“)
    return str
}
println “Rule ran by:  ${operation.user.fullName}”
def uniquerowset = new HashSet<List>()
String CoCode_r = ‘’
String Mgmt_r = ‘’
String Account_r = ‘’
String GBU_r = ‘’
String RevenueType_r = ‘’
String Year_r = ‘’
String Year_rnq=‘’
String Currency_r = ‘’
String Measure_r =‘’
String FcstYr1 = quoteTrim(operation.application.getSubstitutionVariableValue(“FcstYear1"))
List povmbrs = operation.grid.pov
String Scenario_f = povmbrs.find {it.dimName ==‘Scenario’}.essbaseMbrName
String Version_f = povmbrs.find {it.dimName ==‘Version’}.essbaseMbrName
String PE_Adj = rtps.Adj_PlanElement.toString()
	operation.grid.dataCellIterator({DataCell cell -> cell.edited}).each {
      Account_r = ‘“’ + it.getMemberName(“Account”) +‘“’
      GBU_r = ‘“’ + it.getMemberName(“GBU”) + ‘“’
      Mgmt_r = ‘“’ + it.getMemberName(“Management”)+ ‘“’
      CoCode_r = ‘“’ + it.getMemberName(“CompanyCode”)+ ‘“’
      Year_r = ‘“’ + it.getMemberName(“Years”) + ‘“’
      Currency_r = ‘“’ + it.getMemberName(“Currency”)+ ‘“’
      Measure_r = ‘“’ + it.getMemberName(“Measure”)+ ‘“’
      RevenueType_r = ‘“’ + it.getMemberName(“RevenueType”) + ‘“’
      uniquerowset.add([Account_r,GBU_r,Mgmt_r,CoCode_r,Year_r,Currency_r,Measure_r,RevenueType_r])
  	}
if(uniquerowset){
StringBuilder CalcSrpt = StringBuilder.newInstance()
CalcSrpt <<“”"FIX($Scenario_f,$Version_f,&PlanYear)
    %Template (name := “Pop_ARRAY_Input_To_USD_Groovy”, application := “BAXPLAN1", plantype := “PRODUCT”, dtps := ())
    “”"
    uniquerowset.each{
                def A = []
                A = it as List
                Account_r = A.get(0)
                GBU_r = A.get(1)
                Mgmt_r = A.get(2)
                CoCode_r = A.get(3)
                Year_r = A.get(4)
                Currency_r = A.get(5)
                Measure_r = A.get(6)
                RevenueType_r = A.get(7)
                Year_rnq = quoteTrim(Year_r)
                switch (Scenario_f){
                    case ‘Forecast’:
                        switch (Year_rnq){
                            case FcstYr1:
                                  CalcSrpt <<“”" FIX( $Account_r, $GBU_r, $Mgmt_r, $CoCode_r, $Year_r, $Currency_r, $Measure_r, $RevenueType_r  )
                                    FIX(&FcstStartMonth:“Dec”)
                                     $PE_Adj = (“Target_PlanElement” - (“Total_PlanElement” - $PE_Adj))/((“Target_PlanElement” - (“Total_PlanElement” - $PE_Adj))/(“Target_PlanElement” - (“Total_PlanElement” - $PE_Adj)));
                                    ENDFIX
                                    FIX(“Period_Comment”, “User_ID”)
                                     $PE_Adj = “Target_PlanElement”;
                                    ENDFIX
                                  ENDFIX
                                “”"
                            break;
                            default :
                                CalcSrpt <<“”" FIX( $Account_r, $GBU_r, $Mgmt_r, $CoCode_r, $Year_r, $Currency_r, $Measure_r, $RevenueType_r  )
                                  FIX(“Jan”:“Dec”)
                                   $PE_Adj = (“Target_PlanElement” - (“Total_PlanElement” - $PE_Adj))/((“Target_PlanElement” - (“Total_PlanElement” - $PE_Adj))/(“Target_PlanElement” - (“Total_PlanElement” - $PE_Adj)));
                                  ENDFIX
                                  FIX(“Period_Comment”, “User_ID”)
                                   $PE_Adj = “Target_PlanElement”;
                                  ENDFIX
                                ENDFIX
                              “”"
                            break;
                            }
				break;
                default :
                  CalcSrpt <<“”" FIX( $Account_r, $GBU_r, $Mgmt_r, $CoCode_r, $Year_r, $Currency_r, $Measure_r, $RevenueType_r  )
                    FIX(“Jan”:“Dec”)
                     $PE_Adj = (“Target_PlanElement” - (“Total_PlanElement” - $PE_Adj))/((“Target_PlanElement” - (“Total_PlanElement” - $PE_Adj))/(“Target_PlanElement” - (“Total_PlanElement” - $PE_Adj)));
                    ENDFIX
                    FIX(“Period_Comment”, “User_ID”)
                     $PE_Adj = “Target_PlanElement”;
                    ENDFIX
                  ENDFIX
                “”"
                }
    }
    uniquerowset.each{
                def A = []
                A = it as List
                Account_r = A.get(0)
                GBU_r = A.get(1)
                Mgmt_r = A.get(2)
                CoCode_r = A.get(3)
                Year_r = A.get(4)
                Measure_r = A.get(6)
                RevenueType_r = A.get(7)
                Year_rnq = quoteTrim(Year_r)
                switch (Scenario_f){
                    case ‘Forecast’:
                        switch (Year_rnq){
                            case FcstYr1:
                            	CalcSrpt <<“”"%Template (name := “Input_To_USD_Reporting_Currency_Conversion_Groovy”, application := “BAXPLAN1", plantype := “PRODUCT”, dtps := (“Plan_Elements” := [[$PE_Adj]], “Accounts” := [[$Account_r]], “RevenueTypes” := [[“Base”]], “GBUs” := [[$GBU_r]], “Mgmt” := [[$Mgmt_r]], “CoCodes” := [[$CoCode_r]], “Periods” := [[&FcstStartMonth:“Dec”]], “Year” := [[$Year_r]]))
                							  %Template (name := “USD_Rep_To_Functional_Rep_Currency_Conversion_Groovy”, application := “BAXPLAN1”, plantype := “PRODUCT”, dtps := (“Plan_Elements” := [[$PE_Adj]], “Accounts” := [[$Account_r]], “RevenueTypes” := [[“Base”]], “GBUs” := [[$GBU_r]], “Mgmt” := [[$Mgmt_r]], “CoCodes” := [[$CoCode_r]], “Periods” := [[&FcstStartMonth:“Dec”]], “Year” := [[$Year_r]]))“”"
							break;
                            default :
                                CalcSrpt <<“”"%Template (name := “Input_To_USD_Reporting_Currency_Conversion_Groovy”, application := “BAXPLAN1", plantype := “PRODUCT”, dtps := (“Plan_Elements” := [[$PE_Adj]], “Accounts” := [[$Account_r]], “RevenueTypes” := [[“Base”]], “GBUs” := [[$GBU_r]], “Mgmt” := [[$Mgmt_r]], “CoCodes” := [[$CoCode_r]], “Periods” := [[“Jan”:“Dec”]], “Year” := [[$Year_r]]))
                    						  %Template (name := “USD_Rep_To_Functional_Rep_Currency_Conversion_Groovy”, application := “BAXPLAN1", plantype := “PRODUCT”, dtps := (“Plan_Elements” := [[$PE_Adj]], “Accounts” := [[$Account_r]], “RevenueTypes” := [[“Base”]], “GBUs” := [[$GBU_r]], “Mgmt” := [[$Mgmt_r]], “CoCodes” := [[$CoCode_r]], “Periods” := [[“Jan”:“Dec”]], “Year” := [[$Year_r]]))“”"
                            break;
                            }
                   default :
                      CalcSrpt <<“”"%Template (name := “Input_To_USD_Reporting_Currency_Conversion_Groovy”, application := “BAXPLAN1", plantype := “PRODUCT”, dtps := (“Plan_Elements” := [[$PE_Adj]], “Accounts” := [[$Account_r]], “RevenueTypes” := [[“Base”]], “GBUs” := [[$GBU_r]], “Mgmt” := [[$Mgmt_r]], “CoCodes” := [[$CoCode_r]], “Periods” := [[“Jan”:“Dec”]], “Year” := [[$Year_r]]))
                                    %Template (name := “USD_Rep_To_Functional_Rep_Currency_Conversion_Groovy”, application := “BAXPLAN1", plantype := “PRODUCT”, dtps := (“Plan_Elements” := [[$PE_Adj]], “Accounts” := [[$Account_r]], “RevenueTypes” := [[“Base”]], “GBUs” := [[$GBU_r]], “Mgmt” := [[$Mgmt_r]], “CoCodes” := [[$CoCode_r]], “Periods” := [[“Jan”:“Dec”]], “Year” := [[$Year_r]]))“”"
                   break;
    			}
	}
	CalcSrpt << “”"ENDFIX”“”
//println CalcSrpt
return CalcSrpt
}